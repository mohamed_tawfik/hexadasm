package com.mohamed.hexadasm

data class Movie(val title: String, var rating: Float = 0f)