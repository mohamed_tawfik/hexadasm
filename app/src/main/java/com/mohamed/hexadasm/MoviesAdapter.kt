package com.mohamed.hexadasm

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MoviesAdapter(
    private val data: ArrayList<Movie>,
    private val context: Context, val onRatingChanged: (Int) -> Unit
) : RecyclerView.Adapter<MoviesAdapter.FlagsClaimedViewHolder>() {

    var isRatingEnabled: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlagsClaimedViewHolder {
        return FlagsClaimedViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_movie, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: FlagsClaimedViewHolder, position: Int) {

        holder.title.text = data[position].title
        holder.rate.rating = data[position].rating

        holder.rate.isEnabled = isRatingEnabled
        holder.rate.setOnRatingBarChangeListener { ratingBar, rating, b ->
            if (isRatingEnabled) {
                data[position].rating = rating
                onRatingChanged(position)
            }
        }
    }

    class FlagsClaimedViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val title: TextView = itemView!!.findViewById(R.id.textView)
        val rate: RatingBar = itemView!!.findViewById(R.id.ratingBar)
    }
}