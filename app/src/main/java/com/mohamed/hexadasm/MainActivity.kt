package com.mohamed.hexadasm

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var moviesList: ArrayList<Movie>
    lateinit var compositeDisposable: CompositeDisposable
    lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initMoviesList()

        rvMoviesList.adapter = MoviesAdapter(moviesList, this) { pos ->
            moviesList.sortByDescending { it.rating }
            (rvMoviesList.adapter as MoviesAdapter).notifyDataSetChanged()
        }

        compositeDisposable = CompositeDisposable()
        startRandom()
    }

    private fun initMoviesList() {
        moviesList = ArrayList<Movie>()
        moviesList.add(Movie("Avengers"))
        moviesList.add(Movie("Kong Fu Panda"))
        moviesList.add(Movie("Spider-Man"))
        moviesList.add(Movie("Toy Story"))
        moviesList.add(Movie("Cars"))
        moviesList.add(Movie("Alaadin"))
        moviesList.add(Movie("Moana"))
        moviesList.add(Movie("Finding Nemo"))
        moviesList.add(Movie("Up"))
        moviesList.add(Movie("Incredibles"))
    }

    private fun startRandom() {
        // start random rating from 1-5 seconds
        disposable =
            Observable.interval(0, Random.nextLong(1, 5), TimeUnit.SECONDS)
                .flatMap {
                    return@flatMap Observable.create<Movie> { emitter ->

                        (rvMoviesList.adapter as MoviesAdapter).isRatingEnabled = false

                        val movie = moviesList[Random.nextInt(0, 9)]
                        movie.rating = Random.nextInt(1, 6).toFloat()

                        emitter.onNext(movie)
                        emitter.onComplete()
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    moviesList.sortByDescending { it.rating }
                    (rvMoviesList.adapter as MoviesAdapter).notifyDataSetChanged()
                }

        compositeDisposable.add(disposable)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId!! == R.id.action_random) {

            if (compositeDisposable.size() > 0) {
                compositeDisposable.remove(disposable)
                (rvMoviesList.adapter as MoviesAdapter).isRatingEnabled = true
                (rvMoviesList.adapter as MoviesAdapter).notifyDataSetChanged()
            } else {
                startRandom()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
